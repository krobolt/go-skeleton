[![Go Report Card](https://goreportcard.com/badge/gitlab.com/krobolt/go-skeleton)](https://goreportcard.com/report/gitlab.com/krobolt/go-skeleton)

# Go-Skeleton

Skeleton web framework for go.

Example site can be found in example folder or downloaded from the example branch

## Components

### Router

[Go-Router](https://gitlab.com/krobolt/go-router)

Regex pattern matching router.

### Middleware Dispatcher

[Go-Dispatcher](https://gitlab.com/krobolt/go-dispatcher)

Middleware dispatcher using [Go-Router](https://gitlab.com/krobolt/go-router)

### Go-Overlay

[Go-Overlay](https://gitlab.com/krobolt/go-overlay)

Go template engine

## Other Components

[Go-Strorage](https://gitlab.com/krobolt/go-storage)

DB/Reddis component.
