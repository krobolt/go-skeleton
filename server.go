package skeleton

import (
	"bufio"
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
)

var (
	c chan os.Signal
)

//NewHTTPServer returns instance of http.Server.
func NewHTTPServer(httpAddr string, mux *http.ServeMux) *http.Server {
	return &http.Server{
		Addr:         httpAddr,
		Handler:      mux,
		WriteTimeout: WriteTimeoutFlag,
		ReadTimeout:  ReadTimeoutFlag,
		IdleTimeout:  IdleTimeoutFlag,
	}
}

//ServeSite runs server
func ServeSite(s Site) {

	t := s.Load()
	LoadTemplate(t)

	httpServer := s.NewHTTPServer()
	go refreshTemplate(s)

	log.Println("Server shutdown delay:", WaitFlag)
	log.Println("HTTP writetimeout:", WriteTimeoutFlag)
	log.Println("HTTP readtimeout:", ReadTimeoutFlag)
	log.Println("HTTP idletimeout:", IdleTimeoutFlag)
	log.Printf("Dev Mode: %v \n\n", DevMode)
	log.Println("Starting Server: ", httpServer.Addr)

	go func() {
		if err := httpServer.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	gracefulShutdown(httpServer)
}

func refreshTemplate(s Site) {
	if !DevMode {
		return
	}
	for {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			st := scanner.Text()
			if st == "r" || st == "reload" {
				log.Println("Notice: refreshing template views")
				LoadTemplate(s.Load())
				continue
			}
		}
	}
}

func gracefulShutdown(httpServer *http.Server) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	ctx, cancel := context.WithTimeout(context.Background(), WaitFlag)
	defer cancel()
	httpServer.Shutdown(ctx)
	log.Println("Notice: Shutting Down Server:", httpServer.Addr)
}
