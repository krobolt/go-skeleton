package examplealt

import (
	"html"
	"log"
	"net/http"
	"net/http/pprof"
	"path/filepath"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	dispatcher "gitlab.com/krobolt/go-dispatcher"
	overlay "gitlab.com/krobolt/go-overlay"
	"gitlab.com/krobolt/go-router"
	"gitlab.com/krobolt/go-skeleton"
)

//Website example
type Website struct {
	address string
	static  string
}

//NewExampleWebsite returns instance of example website running on port 80 with '/static' static fileserver
//use by running skeleton.ServeSite(NewExampleWebsite())
func NewExampleWebsite(addy string) *Website {
	return &Website{
		address: addy,
		static:  "static",
	}
}

//Start main function
func Start() {
	skeleton.InitFlags()
	skeleton.ServeSite(NewExampleWebsite(":80"))
}

//Load template files
func (w *Website) Load() overlay.Overlay {
	t, err := overlay.NewTheme(
		"master.html",
		filepath.Join(filepath.Dir(""), "views"),
		"layouts",
	)
	if err != nil {
		//no point in going any further without template files.
		panic(err)
	}
	return t
}

//Dispatcher handles routes and middleware execution
func (w *Website) Dispatcher() dispatcher.Dispatcher {

	d := dispatcher.NewDispatcher(
		"/",
		router.NewDefaultOptions(),
		[]string{"global"}, //global names applied to all routes
	)

	//define middleware on named routes
	d.SetMiddleware("global", skeleton.LoggingMiddleware)

	//update/set custom error handles
	d.AddHandle(404, New404)
	d.AddHandle(500, New500)

	d.Add("GET", "/", homeHandler, []string{"homepage"})
	d.Add("POST", "/action", postExample, []string{"json"})

	d.Group("/dir", func() {
		//index of /dir
		d.Add("GET", "/", dirHandler, nil)
	}, []string{"middlewarename"})

	return d
}

//NewHTTPServer returns *http.Server
func (ew *Website) NewHTTPServer() *http.Server {

	var (
		fs  = http.FileServer(http.Dir(ew.static))
		mux = http.NewServeMux()
	)

	log.Println("Starting Server:", time.Now().Unix())

	mux.Handle("/", ew.Dispatcher())
	mux.Handle("/static/", http.StripPrefix("/"+ew.static+"/", fs))

	//promhttp metrics
	mux.Handle("/metrics", promhttp.Handler())

	if skeleton.DevMode == true {
		//debugging tools dev mode
		mux.HandleFunc("/debug/pprof/", pprof.Index)
		mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
		mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
		mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
		mux.HandleFunc("/debug/pprof/trace", pprof.Trace)
	}

	return skeleton.NewHTTPServer(ew.address, mux)
}

//New404 not found error page
func New404(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(404)
	skeleton.Render(w, "404.html", nil)
}

//New500 internal server error page
func New500(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(500)
	skeleton.Render(w, "500.html", nil)
}

//RenderStatic decorates skeleton.Renderstic handles error with custom 500 page
func RenderStatic(w http.ResponseWriter, r *http.Request, layout string, tag string) {
	err := skeleton.RenderStatic(w, r, layout, tag)
	if err != nil {
		New500(w, r)
	}
}

//pushFile HTTP 2.0 push example
func pushFile(w http.ResponseWriter, r *http.Request) {
	pusher, ok := w.(http.Pusher)
	if ok {
		options := &http.PushOptions{
			Header: http.Header{
				"Accept-Encoding": r.Header["Accept-Encoding"],
			},
		}
		if err := pusher.Push("/static/main.css", options); err != nil {
			log.Printf("Failed to push: %v", err)
		}
	}
}

//homeHandler example handler
func homeHandler(w http.ResponseWriter, r *http.Request) {
	RenderStatic(w, r, "homepage.html", "160221")
}

//dirHandler example handler
func dirHandler(w http.ResponseWriter, r *http.Request) {
	RenderStatic(w, r, "dirhandler.html", "160221")
}

//postExample checks method, reads submitted inputs and outputs JSON Response
func postExample(w http.ResponseWriter, r *http.Request) {
	path := html.EscapeString(r.URL.Path)

	//set default response
	response := skeleton.NewResponse()
	response.Code = http.StatusUnauthorized

	defer func() {
		//handle errors/panics and/or respond at the end
		if r := recover(); r != nil {
			log.Printf("Unable to satisfy request: %s. Recovery: %s \n", path, r)
		}
		response.Respond(w)
	}()

	//reject non POST methods
	if r.Method != http.MethodPost {
		response.Error = "method not allowed, only POST supported"
		response.Code = http.StatusMethodNotAllowed
		return
	}

	//parse inputs
	inputs, err := skeleton.GetInputs(r, "username", "password")
	if err != nil {
		response.Error = err.Error()
		response.Code = http.StatusUnprocessableEntity
		return
	}

	//update the response
	response.Data = inputs["username"]
	response.Code = http.StatusAccepted
	response.Error = ""
}
