package skeleton

import (
	"crypto/tls"
	"fmt"
	"net/smtp"
	"os"
	"strings"
)

type Mailer struct {
	Sender  string
	To      []string
	Subject string
	Body    string
}

type SmtpServer struct {
	Host string
	Port string
}

func (s *SmtpServer) Name() string {
	return s.Host + ":" + s.Port
}

func (mail *Mailer) buildMessage() string {

	message := ""
	message += fmt.Sprintf("From: %s\r\n", mail.Sender)

	if len(mail.To) > 0 {
		message += fmt.Sprintf("To: %s\r\n", strings.Join(mail.To, ";"))
	}

	message += fmt.Sprintf("Subject: %s\r\n", mail.Subject)
	message += "\r\n" + mail.Body

	return message
}

func NewMail(subject, body string, to []string) error {
	mail := Mailer{}
	mail.Sender = os.Getenv("EMAIL_ADDY")
	mail.To = to
	mail.Subject = subject
	mail.Body = body
	messageBody := mail.buildMessage()

	smtpServer := SmtpServer{
		Host: os.Getenv("EMAIL_HOST"),
		Port: os.Getenv("EMAIL_PORT"),
	}

	auth := smtp.PlainAuth("", mail.Sender, os.Getenv("EMAIL_PASSWORD"), smtpServer.Host)
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         smtpServer.Host,
	}
	conn, err := tls.Dial("tcp", smtpServer.Name(), tlsconfig)
	if err != nil {
		return err
	}

	client, err := smtp.NewClient(conn, smtpServer.Host)
	if err != nil {
		return err
	}

	// step 1: Use Auth
	if err = client.Auth(auth); err != nil {
		return err
	}

	// step 2: add all from and to
	if err = client.Mail(mail.Sender); err != nil {
		return err
	}
	for _, k := range mail.To {
		if err = client.Rcpt(k); err != nil {
			return err
		}
	}
	w, err := client.Data()
	if err != nil {
		return err
	}
	_, err = w.Write([]byte(messageBody))
	if err != nil {
		return err
	}
	err = w.Close()
	if err != nil {
		return err
	}
	client.Quit()
	return nil
}
