package skeleton

import (
	"log"
	"net/http"
	"time"

	dispatcher "gitlab.com/krobolt/go-dispatcher"
)

//LoggingMiddleware log response load time
func LoggingMiddleware() dispatcher.Middleware {
	// Create a new Middleware
	return func(f http.HandlerFunc) http.HandlerFunc {
		// Define the http.HandlerFunc
		return func(w http.ResponseWriter, r *http.Request) {
			// Do middleware things
			start := time.Now()
			defer func() { log.Println(time.Since(start)) }()
			// Call the next middleware/handler in chain
			f(w, r)
		}
	}
}
